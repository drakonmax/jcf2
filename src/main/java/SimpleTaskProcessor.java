public class SimpleTaskProcessor implements ITaskProcessor{
    private IBuffer buffer;


    public SimpleTaskProcessor(IBuffer buffer) {
        if (buffer == null)
            throw new NullPointerException("Null buffer");
        this.buffer = buffer;
    }


    @Override
    public Integer process() {
        if (buffer.quantity() == 0)
            return null;

        int sum = 0;
        for (int number: buffer.getElem().getData()) {
            sum += number;
        }

        return sum;
    }
}

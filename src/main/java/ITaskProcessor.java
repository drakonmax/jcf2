public interface ITaskProcessor {
    Integer process();
}

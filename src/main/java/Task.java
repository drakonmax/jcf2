import java.util.Arrays;

public class Task implements ITask{

    private int[] coeffs;


    public Task(int... coeffs) {
        this.coeffs = coeffs;
    }

    @Override
    public int[] getData() {
        return coeffs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Arrays.equals(coeffs, task.coeffs);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(coeffs);
    }
}

public interface IBuffer {
    void addElem(Task elem);
    Task getElem();
    int quantity();
    void clear();
    boolean isEmpty();

}

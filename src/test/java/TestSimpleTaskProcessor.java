import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestSimpleTaskProcessor {
    @Test
    void testSimpleTaskProcessor() {
        assertDoesNotThrow(() -> new SimpleTaskProcessor(new SimpleBuffer()));
        try{
            SimpleTaskProcessor processor = new SimpleTaskProcessor(null);
        }
        catch (NullPointerException e){
            assertEquals("Null buffer", e.getMessage());
        }
    }

    @Test
    void testProcessEmptyBuffer() {
        SimpleTaskProcessor processor1 = new SimpleTaskProcessor(new SimpleBuffer());

        assertNull(processor1.process());
    }

    @Test
    void testProcessOneTask() {
        SimpleBuffer buffer = new SimpleBuffer();
        SimpleTaskProcessor processor = new SimpleTaskProcessor(buffer);

        buffer.addElem(new Task(5, -7, 23));

        assertEquals(21, processor.process());
        assertNull(processor.process());
    }

    @Test
    void testProcessTwoTasks() {
        SimpleBuffer buffer = new SimpleBuffer();
        SimpleTaskProcessor processor = new SimpleTaskProcessor(buffer);

        buffer.addElem(new Task(11, 2, -12));
        new SimpleTaskGenerator(buffer, 1, 5).generate();

        assertEquals(1, processor.process());
        assertEquals(15, processor.process());
        assertNull(processor.process());
    }
}

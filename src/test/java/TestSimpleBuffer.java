import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestSimpleBuffer {

    @Test
    void testQuantityAndIsEmpty() {
        IBuffer buffer = new SimpleBuffer();
        SimpleBuffer buffer1 = new SimpleBuffer();

        assertEquals(0, buffer.quantity());
        assertEquals(0, buffer1.quantity());
        assertTrue(buffer.isEmpty());
        assertTrue(buffer1.isEmpty());
    }
    @Test
    void testAddGetElem() {
        Task task1 = new Task();
        ITask task2 = new Task(1,2,3);
        Task task3 = new Task(17, 8, -4, 1, 31);

        IBuffer buffer = new SimpleBuffer();

        assertTrue(buffer.isEmpty());
        assertEquals(0, buffer.quantity());

        buffer.addElem(task1);
        buffer.addElem((Task) task2);

        assertFalse(buffer.isEmpty());
        assertEquals(2, buffer.quantity());

        buffer.addElem(task3);

        assertFalse(buffer.isEmpty());
        assertEquals(3, buffer.quantity());
        assertEquals(task1, buffer.getElem());
        assertEquals(task2, buffer.getElem());
        assertEquals(task3, buffer.getElem());
        assertEquals(0, buffer.quantity());
        assertTrue(buffer.isEmpty());
    }


    @Test
    void testClear() {
        Task task1 = new Task();
        ITask task2 = new Task(1,2,3);
        Task task3 = new Task(17, 8, -4, 1, 31);

        SimpleBuffer buffer = new SimpleBuffer();

        buffer.addElem(task1);
        buffer.addElem((Task) task2);
        buffer.addElem(task3);

        buffer.clear();

        assertEquals(0, buffer.quantity());
        assertTrue(buffer.isEmpty());
    }
}

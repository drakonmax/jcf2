import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestTask {
    @Test
    void construcrorTest() {

        int[] emptyArray = new int[0];
        int[] array = {1,5,2,6,0,-4,-10,-17,-33};

        Task data = new Task(emptyArray);
        Task data1 = new Task(1,2,3);
        Task data2 = new Task(1);
        ITask data3 = new Task();
        Task data4 = new Task(array);

        assertArrayEquals(emptyArray, data.getData());
        assertArrayEquals(new int[]{1,2,3},data1.getData());
        assertArrayEquals(new int[]{1},data2.getData());
        assertArrayEquals(emptyArray,data3.getData());
        assertArrayEquals(array,data4.getData());
    }
}
